//
//  MovieTableViewCell.swift
//  theMovieDB
//
//  Created by Разработчик on 20.08.2020.
//  Copyright © 2020 Разработчик. All rights reserved.
//

import UIKit

class PopularTableViewCell: UITableViewCell {
    @IBOutlet weak var imageCell: UIImageView!
    @IBOutlet weak var tittleCell: UILabel!
    @IBOutlet weak var overviewCell: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
