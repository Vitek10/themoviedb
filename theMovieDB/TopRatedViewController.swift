//
//  TopRatedViewController.swift
//  theMovieDB
//
//  Created by Разработчик on 20.08.2020.
//  Copyright © 2020 Разработчик. All rights reserved.
//

import UIKit

class TopRatedViewController: UIViewController,UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
        private var movies = [Movie]()
        final let url =
        URL(string: "https://api.themoviedb.org/3/movie/top_rated?api_key=fe12fa80b4d0cb943986570d7d1aec6c&language=en-US&page=1")
        
        override func viewDidLoad() {
            super.viewDidLoad()
            downloadJson()
        }
        
        func downloadJson() {
            guard let downloadURL = url else { return }
            URLSession.shared.dataTask(with: downloadURL) { data, urlResponse, error in
                guard let data = data, error == nil, urlResponse != nil else {
                    return
                }
                do
                {
                    let decoder = JSONDecoder()
                    let downloadedMovies = try decoder.decode(MovieDatabase.self, from: data)
                    self.movies = downloadedMovies.movies
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                } catch {
                    print("something wrong after downloaded")
                }
            }.resume()
        }
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
               return movies.count
           }
           
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
               guard let cell = tableView.dequeueReusableCell(withIdentifier: "topRatedCell") as? TopRatedTableViewCell else { return UITableViewCell() }
               
               cell.tittleCell.text = movies[indexPath.row].title
               cell.overviewCell.text = movies[indexPath.row].overview
               
               if let imageURL = URL(string: "https://image.tmdb.org/t/p/w300" + movies[indexPath.row].imagePoster!) {
                   DispatchQueue.global().async {
                       let data = try? Data(contentsOf: imageURL)
                       if let data = data {
                           let image = UIImage(data: data)
                           DispatchQueue.main.async {
                               cell.imageCell.image = image
                           }
                       }
                   }
               }
               return cell
           }

    }


