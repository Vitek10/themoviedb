//
//  ViewController.swift
//  theMovieDB
//
//  Created by Разработчик on 19.08.2020.
//  Copyright © 2020 Разработчик. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var popularView: UIView!
    @IBOutlet weak var UpcomingView: UIView!
    @IBOutlet weak var topRatedView: UIView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBAction func changeView(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
                  case 0:
                      popularView.isHidden = false
                      UpcomingView.isHidden = true
                      topRatedView.isHidden = true
                  case 1:
                      popularView.isHidden = true
                      UpcomingView.isHidden = false
                      topRatedView.isHidden = true
                  case 2:
                      popularView.isHidden = true
                      UpcomingView.isHidden = true
                      topRatedView.isHidden = false
                  default:
                      break;
            }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator.startAnimating()
        Timer.scheduledTimer(withTimeInterval: 3.0, repeats: true) { timer in
            self.activityIndicator.stopAnimating()
            self.activityIndicator.isHidden = true
        }
    }
}

