//
//  Model.swift
//  theMovieDB
//
//  Created by Разработчик on 20.08.2020.
//  Copyright © 2020 Разработчик. All rights reserved.
//

import Foundation

struct MovieDatabase:Decodable{
    let movies:[Movie]
    private enum CodingKeys: String, CodingKey{
           case movies = "results"
       }
}

struct Movie:Decodable {
    let imagePoster: String?
    let title: String?
    let overview: String?
    
    private enum CodingKeys: String, CodingKey{
        case imagePoster = "poster_path"
        case title
        case overview
    }
    
}
